ELK + MQTT
==========

```
 mqtt client
     |
 mqtt broker (i.e. eclipse-mosquitto)
     |
  logstash
     |
elastic search  —  [db management ui (elasticsearch-head)]
     |
   kibana
```

mqtt broker + logstash + elasticsearch
-----
`docker-compose up` 

give it some time (kibana needs quite some time to start)

mqttclient
----------

`cd mqtt-client`

`docker build -t mqttclient .`

`docker run --rm -it --network elkmqtt_iot_net mqttclient`

`mosquitto_pub -h mqtt-broker -t topic -m "{\"value1\":1, \"value2\":9999}"`

kibana
------
[http://localhost:5601/]()

Indices naming pattern: `logstash-%{[topic]}-%{+YYYY.MM.dd}` 

elasticsearch head
------------------
`docker run -p 9100:9100 --name elasticsearch-head mobz/elasticsearch-head:5`

[http://localhost:9100/]()

Elasticsearch URL: `http://localhost:9200/`

notice
------
based on [https://github.com/deviantony/docker-elk]()